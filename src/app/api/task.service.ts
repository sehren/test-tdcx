import { Injectable, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment'
import { AuthenticationService, CredentialsService } from '@app/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService{
  constructor(public http: HttpClient,public authService: AuthenticationService, public router: Router,
    private credentialsService: CredentialsService) { 
  }
  
  httpPost(method: any,url: any, body: any): Observable<any>{
    return new Observable((observer)=>{
      const credential = this.credentialsService.credentials
      const headers = {
        headers: new HttpHeaders().set(
          "Authorization", 'Bearer '+
          credential.token
        )
      };
      if(method == 'POST'){
        this.http.post(environment.serverUrl+url,body,headers).subscribe(resp=>{
          observer.next(resp)
        },err=>{
          if(err.status==401)
          this.logout()
        })
      } else if(method == 'GET'){
        this.http.get(environment.serverUrl+url,headers).subscribe(resp=>{
          observer.next(resp)
        },err=>{
          if(err.status==401)
          this.logout()
        })
      } else if(method == 'PUT'){
        this.http.put(environment.serverUrl+url,body,headers).subscribe(resp=>{
          observer.next(resp)
        },err=>{
          if(err.status==401){
            this.logout();
          }
        })
      } else if(method == 'DELETE'){
        this.http.delete(environment.serverUrl+url,headers).subscribe(resp=>{
          observer.next(resp)
        },err=>{
          if(err.status==401){
            this.logout();
          }
        })
      }
    })
  }
  public getDashboard(): Observable<any>{
    const _method = 'GET'
    const _url = "/dashboard";
    const _body = {};

    return this.httpPost(_method,_url,_body);
  }
  
  public createTask(body: any): Observable<any>{
    const _method = 'POST'
    const _url = "/tasks";
    const _body = body;

    return this.httpPost(_method,_url,_body);
  }
  public getTask(): Observable<any>{
    const _method = 'GET'
    const _url = "/tasks";
    const _body = {};

    return this.httpPost(_method,_url,_body);
  }
  public editTask(id: any,task: any):Observable<any>{
    const _method = "PUT";
    const _url = '/task/'+id
    const _body = task;

    return this.httpPost(_method,_url,_body)
  }
  public deleteTask(id: any):Observable<any>{
    const _method = "DELETE";
    const _url = '/task/'+id
    const _body = {};

    return this.httpPost(_method,_url,_body)
  }
  logout() {
    this.authService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
  }
}
