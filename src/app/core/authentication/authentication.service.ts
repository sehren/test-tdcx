import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Credentials, CredentialsService } from './credentials.service';
import {environment} from '../../../environments/environment'
import { HttpClient } from '@angular/common/http';
export interface LoginContext {
  id: string;
  name: string;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public urlApi : any = environment.serverUrl;
  constructor(private credentialsService: CredentialsService, public http: HttpClient) { }

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.post(this.urlApi+'/login',{name : context.name, apiKey : context.id}).subscribe((resp: any)=>{
        this.credentialsService.setCredentials(resp.token,true,resp.image)
        resolve(resp.token)
      },err=>{
        reject(err)
      })
    })
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }

}
