import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import * as Chart from 'chart.js'
import { CreateTaskComponent } from '@app/create-task/create-task.component';
import { TaskService } from '@app/api/task.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  myChart: any;
  quote: string | undefined;
  isLoading: boolean = false;
  data: any;
  ctx: any;
  dataTask: any = {}
  listTask: any= []
  listSearch: any = []
  search: string;
  listLoading: any = []
  currentChart: any;
  constructor(public dialog: MatDialog, public taskService: TaskService){

  }
  ngAfterViewInit(){
    this.getDashboard()

  }
  defineLoading(number: number){
    this.listLoading = Array(number).fill(0).map((x,i)=>i);
  }
  getDashboard(){
    this.isLoading = true;
    this.taskService.getDashboard().subscribe(resp=>{
      this.isLoading = false;
      this.dataTask = resp;
      this.getListTask()
    })
  }
  refreshTask(){
    this.isLoading = true;
    this.taskService.getDashboard().subscribe(resp=>{
      this.dataTask = resp;
      this.getListTask()
    })
  }
  checkedTask(index: any,value: any){
    this.listTask[index].completed = value.checked;
    this.editTask(this.listTask[index])
  }
  editTask(task: any){
    this.defineLoading(this.listTask.length)
    this.isLoading = true;
    this.taskService.editTask(task._id,task).subscribe(resp=>{
      this.refreshTask();
    })
  }
  openDialogEdit(task: any){
    const openModal = this.dialog.open(CreateTaskComponent,{
      width : '300px',
      backdropClass : 'customModal',
      data: {nameTask: task.name}
    })
    openModal.afterClosed().subscribe(resp=>{
      if(resp){
        task.name = resp;
        this.editTask(task)
      }
    })
  }
  deleteTask(task: any){
    this.defineLoading(this.listTask.length-1)
    this.isLoading = true;
    this.taskService.deleteTask(task._id).subscribe(resp=>{
      this.refreshTask()
    })
  }
  openDialog(name: string = null){
    const openModal = this.dialog.open(CreateTaskComponent,{
      width : '300px',
      backdropClass : 'customModal',
      data: {nameTask: name}
    })
    openModal.afterClosed().subscribe(resp=>{
      if(resp){
        this.search = ""
        this.defineLoading(this.listTask.length+1)
        this.isLoading = true;
        this.taskService.createTask({name : resp}).subscribe(res=>{
          this.refreshTask()
        })
      }
    })
  }
  getListTask(){
    this.isLoading = true;
    this.taskService.getTask().subscribe(resp=>{
      this.listTask = resp.tasks;
      this.listSearch = resp.tasks;
      this.isLoading = false;
      if(this.dataTask.totalTasks>0){
        if(this.currentChart){
          console.log(this.currentChart)
          let complete = this.dataTask.tasksCompleted
          let total = this.dataTask.totalTasks
          let percentage = complete*100/total
          let data = [percentage,100-percentage]
          this.updateChart(data)
        }
        else{
          this.pieChart()
        }
      }
      else{
        this.currentChart = null;
      }
    })
  }
  inputSearch(){
    this.listTask = this.listSearch
    let temp = this.listTask.filter((i:any)=>{
      if(i.name.toLowerCase().indexOf(this.search) > -1){
        return true
      }
    })
    this.listTask = temp;
  }
  pieChart(){
    let complete = this.dataTask.tasksCompleted
    let total = this.dataTask.totalTasks
    let percentage = complete*100/total
    const data = {
      labels: ["Completed Task", 'Unallocated'],
      datasets: [{
          data: [percentage,100-percentage],
          backgroundColor: [
              'rgba(54, 162, 235, 1)',
              'rgba(247, 236, 235)',
          ],
          borderWidth: 1
      }]
    }
    this.myChart = document.getElementById('myChart');
    this.ctx = this.myChart.getContext('2d');
    this.currentChart = new Chart(this.ctx, {
      type: 'pie',
      data: data,
      options: {
        responsive: false,
        legend : {
          display : false
        },
        tooltips : {
          enabled : false
        }
      }
    });

  }
  updateChart(data: any){
    this.currentChart.data.datasets[0].data = data;
    this.currentChart.update();
  }
}

